ARG BASE_VERSION=latest

FROM alpine:${BASE_VERSION}

RUN apk add --no-cache git jq curl